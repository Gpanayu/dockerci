#!/bin/bash
docker stop db;
docker container rm db;
docker run --name db -d -v ${pwd}:/app -e POSTGRES_PASSWORD=1234567890 -p 5433:5432 -e POSTGRES_DB=SE postgres;

docker exec db psql -f /app/database/create_table.sql -U postgres -d SE;

docker stop sepj;
docker container rm sepj;
docker run --name sepj -it -v ${pwd}:/app --link db:postgres -p 5000:5000 python bin/bash -c "/app/initBack.sh";
