from entity.transaction import Transaction
from database import db
from flask import session

class TransactionHistory:
    def __init__(self):
        # please delete this function
        print("created")
    def addTransaction(self, transaction):
        print("do something")
    def removeTransaction(self, TID):
        UID = session['UID']
        transaction = Transaction()
        transaction.setTID(TID)
        count = 0
        type = []
        sql = """SELECT "JID" FROM public."Receive" WHERE "UID" = '"""+UID+"""' AND "JID" = '"""+transaction.getTID()+"""';"""
        TIDs = db.select(sql)
        count += len(TIDs)
        if len(TIDs) != 0:
            type.append(0)
        sql = """SELECT "TID" FROM public."Transfer" WHERE "UID" = '"""+UID+"""' AND "TID" = '"""+transaction.getTID()+"""';"""
        TIDs = db.select(sql)
        count += len(TIDs)
        if len(TIDs) != 0:
            type.append(1)
        if count == 0:
            return False
        sql = """DELETE FROM public."Transaction" WHERE "TID" = '"""+transaction.getTID()+"';"
        db.delete(sql)
        for i in range(len(type)):
            if type[i] == 0:
                sql = """DELETE FROM public."Receive" WHERE "UID" = '"""+UID+"""' AND "JID" = '"""+transaction.getTID()+"""';"""
                db.delete(sql)
            if type[i] == 1:
                sql = """DELETE FROM public."Transfer" WHERE "UID" = '"""+UID+"""' AND "TID" = '"""+transaction.getTID()+"""';"""
                db.delete(sql)
        del transaction
        return True
    def getTransaction(self):
        UID = session['UID']
        sql = """SELECT "JID" FROM public."Receive" WHERE "UID" = '"""+UID+"';"
        TIDs = db.select(sql)
        sql = """SELECT "TID" FROM public."Transfer" WHERE "UID" = '"""+UID+"';"
        output = db.select(sql)
        for i in range(len(output)):
            TIDs.append(output[i])
        results = []
        for i in range(len(TIDs)):
            print(TIDs[i][0])
            sql = """SELECT "TID", "ReceiveID", "Type", "Amount", "PayID", "DateTime" FROM public."Transaction" WHERE "TID" = '"""+TIDs[i][0]+"';"
            output = db.select(sql)
            print(str(output))
            r = {}
            if len(output) != 0:
                r['TID'] = output[0][0]
                r['ReceiveID'] = output[0][1]
                r['Type'] = output[0][2]
                r['Amount'] = output[0][3]
                r['PayID'] = output[0][4]
                r['DateTime'] = output[0][5]
                print(str(r))
                results.append(r)
        return results
    def getTransactionDetail(self, TID):
        sql = """SELECT "TID", "ReceiveID", "Type", "Amount", "PayID", "DateTime" FROM public."Transaction" WHERE "TID" = '"""+TID+"""';"""
        output = db.select(sql)
        print("output is this it is = "+str(output))
        obj = {}
        if len(output) == 0:
            return
        obj['TID'] = output[0][0]
        obj['ReceiveID'] = output[0][1]
        obj['Type'] = output[0][2]
        obj['Amount'] = output[0][3]
        obj['PayID'] = output[0][4]
        obj['DateTime'] = output[0][5]
        return obj
