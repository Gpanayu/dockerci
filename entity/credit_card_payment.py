from entity.card_receipt import CardReceipt
import os, stripe
from flask import request

class CreditCardPayment:
    def __init__(self): 
        self.stripe_keys = {'secret_key': 'sk_test_kwd6ywczqCu8UlN8xfjeCjlX', 'publishable_key': 'pk_test_nSKBz5raT0ehYmpCd8bQlSGx'}
        stripe.api_key = self.stripe_keys['secret_key']
    
    def pay(self, credit_card, amount, _id, email):
        charge = stripe.Charge.create(
            customer=_id,
            amount=amount,
            currency='usd',
            description='Charge'
        )

        if charge["status"] == 'succeeded':
            card_receipt = CardReceipt('tmpdate', amount, credit_card.holderName, credit_card.cardNumber, credit_card.bank)
            return card_receipt, True
        else:
            return None, False
