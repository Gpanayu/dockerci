from flask import session
from database import db
import datetime

class Job:
    def __init__(self, JID = None):
        if JID is None:
            sql = """SELECT COUNT(*) FROM public."Job";"""
            count = db.select(sql)[0][0]
            self.__JID = count
        else:
            self.__JID = JID
            sql = """SELECT "JID", "Title", "Deadline", "CreatedDate", "Description", "Owner", "Status", "Budget"
                     FROM public."Job"
                     WHERE "JID" = '"""+str(JID)+"';"
            query = db.select(sql)[0]
            self.__title = query[1]
            self.__deadline = query[2]
            self.__createDate = query[3]
            self.__description = query[4]
            self.__ownerUID = query[5]
            self.__status = query[6]
            self.__budget = query[7]
        print("created")


    def createJob(self, title, owner, budget, deadline, description):
        self.__title = title
        self.__ownerUID = owner
        self.__budget = budget
        self.__deadline = deadline
        self.__description = description
        self.__status = "Available"
        self.__createDate = datetime.datetime.now()
        sql = """INSERT INTO public."Job"("JID", "Title", "Deadline", "CreatedDate", "Description", "Owner", "Status", "Budget") \
                       VALUES ("""+str(self.__JID)+", '"+self.__title+"', '"+self.__deadline+"', '"+str(self.__createDate)+"', '"+self.__description+"', '"+str(self.__ownerUID)+"', '"+self.__status+"', "+self.__budget+""");"""
        status = db.insert(sql)
        return self.__JID

    def request(self, UID):
        sql = """INSERT INTO public."Request"("UID", "JID")	VALUES ('"""+UID+"', '"+self.__JID+"');"
        db.insert(sql)

    def cancelRequestJob(self, UID):
        sql = """DELETE FROM public."Request" WHERE "UID" = '"""+str(UID)+"""' AND "JID" = '"""+str(self.__JID)+"';"
        db.delete(sql)

    def getStatus(self):
        return self.__status

    def updateStatus(self, status):
        sql = """UPDATE public."Job" SET "Status" = '"""+str(status)+"""' WHERE "JID" = '"""+str(self.__JID)+"';"
        return db.update(sql)

    def getOwnerUID(self):
        return self.__ownerUID