from database import db
from flask import current_app as app
from flask_mail import Mail, Message
from secret import Email, Password
class User:
    def __init__(self):
        print("User entity created")
    def register(self, email, username, password):
        self.__email = email
        self.__username = username
        self.__password = password
        self.__verify = True
        sql = """SELECT COUNT(*) FROM public."User";"""
        total = db.select(sql)
        if len(total) == 0:
            total = 0
        else:
            total = total[0][0]
        self.__UID = int(total)+1
        sql = """INSERT INTO public."User"("UID", "isVerified", "Email", "Password", "Username") VALUES ("""+str(self.__UID)+", "+str(self.__verify)+", '"+self.__email+"', '"+self.__password+"', '"+self.__username+"""');"""
        db.insert(sql)
    def registerWithVer(self, email, username, password):
        self.__email = email
        self.__username = username
        self.__password = password
        self.__verify = True
        sql = """SELECT * FROM public."User" WHERE "Email" = '"""+self.__email+"""' OR "Username" = '"""+self.__username+"""';"""
        total = db.select(sql)
        print("total = "+str(len(total)))
        if len(total) != 0:
            # send email
            msg = Message(subject="Please verify email",
                sender=Email, recipients=[self.__email], body="Dear customer,\nPlease verify your email at link: http://localhost:5000/verify?email="+self.__email+"&username="+self.__username+"&password"+self.__password
            )
            mail = Mail(app)
            mail.send(msg)
            return True
        else:
            return False
    def getUID(self):
        return self.__UID
    def loginEmail(self, email, password):
        sql = """SELECT "UID", "isVerified", "LastName", "Picture", "BankAccountNumber", "FirstName", "Age", "CitizenID", "Email", "Password", "Username", "PhoneNumber", "LineID", "FacebookLink", "FacebookID", "FacebookToken" FROM public."User" WHERE "Email" = '"""+email+"""' AND "Password" = '"""+password+"""';"""
        user = db.select(sql)
        if len(user) == 0:
            return False
        else :
            return user[0]
    def loginUsername(self, username, password):
        sql = """SELECT "UID", "isVerified", "LastName", "Picture", "BankAccountNumber", "FirstName", "Age", "CitizenID", "Email", "Password", "Username", "PhoneNumber", "LineID", "FacebookLink", "FacebookID", "FacebookToken" FROM public."User" WHERE "Username" = '"""+username+"""' AND "Password" = '"""+password+"""';"""
        user = db.select(sql)
        if len(user) == 0:
            return False
        else :
            return user[0]

    def getProfile(self, UID):
        sql = """SELECT "isVerified", "UID", "FirstName", "LastName", "Age", "Picture", "Email", "PhoneNumber", "LineID", "FacebookLink", "BankAccountNumber" FROM "User" WHERE "UID"='"""+str(UID)+"""';"""
        result = db.select(sql)
        if (len(result) == 0):
            return False
        else:
            return result[0]

    def updateProfile(self, UID, changeSQL):
        sql = """UPDATE public."User" SET """+changeSQL+""" WHERE "UID" = '"""+str(UID)+"""';"""
        result = db.update(sql)
        if not result:
            return False
        else:
            result = "success"
        return result
