from entity.receipt import Receipt

class ToUserReceipt(Receipt):
    def __init__(self, date, amount, JID, bank, account):
        Receipt.__init__(self, date, amount)
        self.JID = JID
        self.bank = bank
        self.account = account
