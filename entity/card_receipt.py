from entity.receipt import Receipt

class CardReceipt(Receipt):
    def __init__(self, date, amount, cardHolder, cardNumber, bank):
        Receipt.__init__(self, date, amount)
        self.cardHolder = cardHolder
        self.cardNumber = cardNumber
        self.bank = bank