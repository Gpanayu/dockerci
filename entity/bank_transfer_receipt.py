from entity.receipt import Receipt
from database import db

class BankTransferReceipt(Receipt):
    def __init__(self, date, amount, fromBankAccount, toBankAccount, slip, isVerified):
        Receipt.__init__(self, date, amount)
        self.fromBankAccount = fromBankAccount
        self.toBankAccount = toBankAccount
        self.slip = slip
        self.isVerified = isVerified
    def setReceiptID(self, RID):
        self.ReceiptID = RID
    def getReceiptID(self):
        return self.ReceiptID
    def verifyReceipt(self):
        self.isVerified = True
        sql = """UPDATE public."SlipReceipt" SET "isVerify"=true WHERE "ReceiptID" = '"""+self.ReceiptID+"""';"""
        db.update(sql)
