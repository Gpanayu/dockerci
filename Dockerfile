# Use Node v4 as the base image.
FROM python:latest

# You need to add commands for the following sections!!!

# --> Add everything in the current directory to our image, in the 'app' folder.
ADD . /app
ADD secret.py /

# --> Install dependencies
RUN cd /app; \
pip install -r requirements.txt

# --> Expose our server port.
EXPOSE 5000
VOLUME /app

ENV FLASK_ENV=development
ENV FLASK_APP=/app/app.py

# --> Run our app.
CMD flask run --host='0.0.0.0'

