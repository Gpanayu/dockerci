from database import db
from entity import PostedHistory
from entity import User
from flask import session

class UserControl:
    def __init__(self):
        # please delete this function
        print("user controller created")

    def getProfile(self, UID=None):

        if UID is None:
            if not 'UID' in session:
                return {"msg": "Not login", "code": 200}
            UID = session['UID']
        user = User()
        result = user.getProfile(UID)
        del user

        if not result:
            return

        history = []
        postedHistory = PostedHistory(UID)
        for job in postedHistory.getHistory():
            history.append(job[0])
        del postedHistory

        user = {}
        user['isVerified'] = result[0]
        user['UID'] = result[1]
        user['FirstName'] = result[2]
        user['LastName'] = result[3]
        user['Age'] = result[4]
        user['Picture'] = result[5]
        user['Email'] = result[6]
        user['PhoneNumber'] = result[7]
        user['LineID'] = result[8]
        user['FacebookLink'] = result[9]
        user['BankAccountNumber'] = result[10]
        user['jobHistory'] = history
        return user

    def updateProfile(self, req):
        if 'UID' in session:
            user = self.getProfile(session['UID'])
            change = ""
            if (user['FirstName'] != req['FirstName']):
                change = change+"\"FirstName\" = '"+req['FirstName']+"', "

            if ('LastName' in req and user['LastName'] != req['LastName']):
                change = change+"\"LastName\" = '"+req['LastName']+"', "

            if 'Age' in req and (user['Age'] != req['Age']):
                change = change+"\"Age\" = "+str(req['Age'])+", "

            if ('Picture' in req and user['Picture'] != req['Picture']):
                change = change+"\"Picture\" = '"+req['Picture']+"', "

            if ('Email' in req and user['Email'] != req['Email']):
                change = change+"\"Email\" = '"+req['Email']+"', "

            if ('PhoneNumber' in req and user['PhoneNumber'] != req['PhoneNumber']):
                change = change+"\"PhoneNumber\" = '"+req['PhoneNumber']+"', "

            if ('LineID' in req and user['LineID'] != req['LineID']):
                change = change+"\"LineID\" = '"+req['LineID']+"', "

            if ('FacebookLink' in req and user['FacebookLink'] != req['FacebookLink']):
                change = change+"\"FacebookLink\" = '"+req['FacebookLink']+"', "

            if ('BankAccountNumber' in req and user['BankAccountNumber'] != req['BankAccountNumber']):
                change = change+"\"BankAccountNumber\" = '"+req['BankAccountNumber']+"', "

            if 'CitizenID' in req:
                change = change+"\"CitizenID\" = '"+req['CitizenID']+"', "

            if 'Username' in req:
                change = change+"\"Username\" = '"+req['Username']+"', "

            if 'FacebookID' in req:
                change = change+"\"FacebookID\" = '"+req['FacebookID']+"', "

            result = "nothing changed"
            if len(change) > 0:
                change = change[:-2]
                instant = User()
                result = instant.updateProfile(session['UID'], change)
                del instant
            return result
