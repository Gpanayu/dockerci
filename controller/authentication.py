from flask import session
from entity import User

class AuthenticationControl:
    def __init__(self):
        # please delete this function
        print("AuthenticationControl created")
    def login(self, req):
        if 'UID' in session:
            return {"msg": "already logged in", "code": 201 }
        else:
            if 'email' in req:
                result = self.__loginEmail(req['email'], req['password'])
            else:
                result = self.__loginUsername(req['username'], req['password'])
            if not result:
                return False
            session['UID'] = result[0]
            user = {}
            user['UID'] = result[0]
            user['isVerified'] = result[1]
            user['LastName'] = result[2]
            user['Picture'] = result[3]
            user['BankAccountNumber'] = result[4]
            user['FirstName'] = result[5]
            user['Age'] = result[6]
            user['CitizenID'] = result[7]
            user['Email'] = result[8]
            user['Password'] = result[9]
            user['Username'] = result[10]
            user['PhoneNumber'] = result[11]
            user['LineID'] = result[12]
            user['FacebookLink'] = result[13]
            user['FacebookID'] = result[14]
            user['facebookToken'] = result[15]
            return user
    def __loginUsername(self, username, password):
        instance = User()
        user = instance.loginUsername(username, password)
        del instance
        if not user:
            return False
        return user
    def __loginEmail(self, email, password):
        instance = User()
        user = instance.loginEmail(email, password)
        del instance
        if not user:
            return False
        return user
    def __loginFacebook(self, facebookToken):
        # do something
        print("do something")
    def signup(self, userinfo):
        if 'UID' in session:
            return {"msg": "already registered", "code": 201 }
        else:
            user = User()
            user.register(userinfo['email'], userinfo['username'], userinfo['password'])
            print(user.getUID())
            session['UID'] = user.getUID()
            del user
            return {"msg": "successful", "code": 201}
    def signupWithVer(self, userinfo):
        if 'UID' in session:
            return {"msg": "already registered", "code": 201 }
        else:
            user = User()
            res = user.registerWithVer(userinfo['email'], userinfo['username'], userinfo['password'])
            return res
    def logout(self):
        if 'UID' in session:
            session.pop('UID', None)
        return {"msg": "successful", "code": 200}
    def resetPassword(self, username, password):
        # do something
        print("do something")
