from flask import session
from entity import TransactionHistory, BankTransferReceipt, Receipt
from database import db

class TransactionControl:
    def __init__(self):
        # please delete this function
        print("created")
    def showTransactions(self):
        if not 'UID' in session:
            return {"msg": "Please log in", "code": 403 }
        else:
            transactionHistory = TransactionHistory()
            res = transactionHistory.getTransaction()
            del transactionHistory
            return {"msg": "successful", "data": res, "code": 200}
    def removeTransaction(self, req):
        if not 'UID' in session:
            return {"msg": "Please log in", "code": 403 }
        else:
            TID = req['TID']
            transactionHistory = TransactionHistory()
            res = transactionHistory.removeTransaction(TID)
            del transactionHistory
            if res:
                return {"msg": "successful", "code": 201}
            else:
                return {"msg": "transaction not found", "code": 404}
    def verifyReceipt(self, req):
        # Check authority first !!!
        # Must be admin only !!!
        TID = req['TID']
        sql = """SELECT "ReceiptID" FROM public."TransactionReceipt" WHERE "TID" = '"""+TID+"""';"""
        output = db.select(sql)
        if len(output) == 0:
            return {"msg": "transaction not found", "code": 404}
        receiptID = output[0][0]
        sql = """SELECT "ReceiptID", "Date", "Amount", "isVerify", "toBankAccount", "fromBankAccount", "Slip" FROM public."SlipReceipt" WHERE "ReceiptID" = '"""+receiptID+"""';"""
        output = db.select(sql)
        if len(output) == 0:
            return {"msg": "receipt not found", "code": 404}
        # date, amount, fromBankAccount, toBankAccount, slip, isVerified
        bankReceipt = BankTransferReceipt(output[0][1], output[0][2], output[0][5], output[0][4], output[0][6], output[0][3])
        bankReceipt.setReceiptID(receiptID)
        bankReceipt.verifyReceipt()
        del bankReceipt
        return {"msg": "successful", "code": 201}
    def getTransactionDetail(self, TID):
        if not 'UID' in session:
            return {"msg": "Please log in", "code": 403 }
        else:
            transactionHistory = TransactionHistory()
            transaction = transactionHistory.getTransactionDetail(TID)
            del transactionHistory
            return {"data": transaction, "code": 200}
