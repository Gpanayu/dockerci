from flask import session
from database import db
from entity import Job, Phase
from entity import PostedHistory

class JobControl:
    def __init__(self):
        # please delete this function
        print("created")

    def createJob(self, req):
        if 'UID' in session:
            title = req['title']
            owner = session['UID']
            budget = req['budget']
            deadline = req['deadline']
            description = req['description']
            phases = req['phases']
            job = Job()
            JID = job.createJob(title, owner, budget, deadline, description)
            history = PostedHistory(owner)
            addHistoryMSG = history.addHistory(JID)
            for i in range(len(phases)):
                phase = Phase()
                print(str(phases[i]))
                phase.createPhaseFromCreateJob(JID, phases[i]['Number'], phases[i]['Description'], phases[i]['Status'], phases[i]['Title'], phases[i]['Budget'])
                del phase
            return {"msg": addHistoryMSG, "code": 201}
        else:
            return {"msg": "Please log in first", "code": 403 }

    def editJobDetail(self, req):
        if 'UID' in session:
            Title=req['Title']
            Deadline=req['Deadline']
            CreatedDate=req['CreatedDate']
            Description=req['Description']
            Budget=req['Budget']
            if(db.editJobDB(req['JID'],Title,Deadline,CreatedDate,Description,Budget)):
                return {"msg": "successful", "code": 201}
            else:
                return {"msg": "database error", "code": 401}
        else:
            return {"msg": "please login", "code": 301}

    def cancelJob(self, req):
        if 'UID' in session:
            sql = """DELETE FROM public."Job" WHERE "JID"='"""+str(req['JID'])+"""';"""
            if(db.delete(sql)):
                return {"msg": "successful", "code": 201}
            else:
                return {"msg": "database error", "code": 401}
        else:
            return {"msg": "please login", "code": 301}

    def addJobPhase(self, req):
        if 'UID' in session:
            sql = """SELECT max("Number") FROM public."Phase" WHERE "JID"='"""+str(req['JID'])+"""';"""
            numPhase = db.select(sql)
            print(str(numPhase))
            if(numPhase[0][0]==None):
                numPhase=1
            else:
                numPhase=int(numPhase[0][0])+1

            sql = """INSERT INTO public."Phase"("JID", "Number", "Description", "Status", "Budget", "Title")
                    VALUES ('"""+str(req['JID'])+"','"+str(numPhase)+"','"+str(req['Description'])+"','"+str(req['Status'])+"','"+str(req['Budget'])+"','"+str(req['Title'])+"');"
            if(db.insert(sql)):
                return {"msg": "successful", "code": 201}
            else:
                return {"msg": "database error", "code": 401}
        else:
            return {"msg": "please login", "code": 301}

    def removeJobPhase(self, req):
        if 'UID' in session:
            sql = """DELETE FROM public."Phase" WHERE "JID"='"""+str(req['JID'])+"""' AND "Number"='"""+str(req['Number'])+"""';"""
            if(db.delete(sql)):
                return {"msg": "successful", "code": 201}
            else:
                return {"msg": "database error", "code": 401}
        else:
            return {"msg": "please login", "code": 301}

    def clearAllJobPhase(self, req):
        if 'UID' in session:
            sql = """DELETE FROM public."Phase" WHERE "JID"='"""+str(req['JID'])+"""';"""
            if(db.delete(sql)):
                return {"msg": "successful", "code": 201}
            else:
                return {"msg": "database error", "code": 401}
        else:
            return {"msg": "please login", "code": 301}

    def editPhaseJobDetail(self, req):
        if 'UID' in session:
            sql = """UPDATE public."Phase"
                    SET "Description"='"""+str(req['Description'])+"""', "Status"='"""+str(req['Status'])+"""', "Budget"='"""+str(req['Budget'])+"""', "Title"='"""+str(req['Title'])+"""'
                    WHERE "JID"='"""+str(req['JID'])+"""' AND "Number"='"""+str(req['Number'])+"""';"""
            if(db.update(sql)):
                return {"msg": "successful", "code": 201}
            else:
                return {"msg": "database error", "code": 401}
        else:
            return {"msg": "please login", "code": 301}

    def getRecentMyJob(self, req):
        if 'UID' in session:
            sql = """SELECT * FROM public."Job" WHERE "Owner"='"""+str(req['UID'])+"""' ORDER BY "CreatedDate" DESC"""
            recentMyJob = db.select(sql)
            try:
                return recentMyJob
            except:
                return {"msg": "database error", "code": 401}
        else:
            return {"msg": "please login", "code": 301}

    def getAllJob(self, req):
        sql = """SELECT job.*, usr."FirstName", usr."LastName"
                 FROM public."Job" job
                 LEFT JOIN public."User" usr ON job."Owner" = usr."UID"
                 ORDER BY "CreatedDate" DESC;"""
        AllJob = db.select(sql)
        try:
            return AllJob
        except:
            return {"msg": "database error", "code": 401}

    def getDesignerTakeNRequestJob(self, req):
        # I think it will be another good way like get all requestJob and check Status
        if 'UID' in session:
            SearchJob={}
            sql = """SELECT job.*, usr."FirstName", usr."LastName"
                    FROM public."Job" job
                    LEFT JOIN public."TakeJob" takejob ON job."JID" = takejob."JID"
                    JOIN public."User" usr on job."Owner" = usr."UID"
                    WHERE takejob."UID"='"""+req['UID']+"';"
            sql2 = """SELECT job.*, usr."FirstName", usr."LastName"
                    FROM public."Job" job
                    LEFT JOIN public."Request" request ON job."JID" = request."JID"
                    JOIN public."User" usr on job."Owner" = usr."UID"
                    WHERE request."UID"='"""+req['UID']+"';"
            TakeJob = db.select(sql)
            RequestJob = db.select(sql2)
            SearchJob['TakeJob']=TakeJob
            SearchJob['RequestJob']=RequestJob
            return SearchJob
        else:
            return {"msg": "please login", "code": 301}

    def getJob(self, req):
        Job={}
        sql1 = """SELECT * FROM public."Job" WHERE "JID"='"""+req['JID']+"';"
        sql2 = """SELECT * FROM public."Phase" WHERE "JID"='"""+req['JID']+"';"
        sql3 = """SELECT * FROM public."Request" WHERE "JID"='"""+req['JID']+"';"
        sql4 = """SELECT * FROM public."TakeJob" WHERE "JID"='"""+req['JID']+"';"
        sql5 = """SELECT * FROM public."PostJob" WHERE "Job"='"""+req['JID']+"';"
        job = db.select(sql1)
        phase = db.select(sql2)
        request = db.select(sql3)
        takejob = db.select(sql4)
        postjob = db.select(sql5)
        try:
            Job['Job']=job
            Job['Phase']=phase
            Job['Request']=request
            Job['TakeJob']=takejob
            Job['PostJob']=postjob
            return Job
        except:
            return {"msg": "database error", "code": 401}

    def searchJob(self, req):
        # if 'UID' in session:
        sql=""
        if(len(req['Keyword'])==0):
            sql = """SELECT * FROM public."Job";"""
        else:
            sql = """SELECT * FROM public."Job" WHERE LOWER("Title") LIKE '%"""+req['Keyword'].lower()+"""%' OR LOWER("Description") LIKE '%"""+req['Keyword'].lower()+"""%';"""
        Job = db.select(sql)
        try:
            return Job
        except:
            return {"msg": "database error", "code": 401}
        # else:
        #     return {"msg": "please login", "code": 301}

    def getJobRT(self, req):
        if 'UID' in session:
            JobRT={}
            sql = """SELECT * FROM public."Job" WHERE "JID"='"""+req['JID']+"""';"""
            sql2 = """SELECT request."UID", usr."FirstName", usr."LastName"
                    FROM public."Request" request
                    JOIN public."User" usr ON usr."UID"= request."UID"
                    WHERE "JID"='"""+req['JID']+"""';"""
            sql3 = """SELECT takejob."UID", usr."FirstName", usr."LastName"
                    FROM public."TakeJob" takejob
                    JOIN public."User" usr ON usr."UID"= takejob."UID"
                    WHERE "JID"='"""+req['JID']+"""';"""
            Job = db.select(sql)
            Request = db.select(sql2)
            TakeJob = db.select(sql3)
            try:
                JobRT["Job"]=Job
                JobRT["Request"]=Request
                JobRT["TakeJob"]=TakeJob
                return JobRT
            except:
                return {"msg": "database error", "code": 401}
        else:
            return {"msg": "please login", "code": 301}

    def getPendingPhase(self):
        if not 'UID' in session:
            return {"msg": "please login", "code": 301}
        else:
            UID = session['UID']
            sql = """SELECT "JID", "Title", "Deadline", "CreatedDate", "Description", "Owner", "Status", "Budget" FROM public."Job" WHERE "Owner" = '"""+UID+"""';"""
            jobs = db.select(sql)
            data = []
            for i in range(len(jobs)):
                currentJob = jobs[i]
                JID = currentJob[0]
                sql = """SELECT "JID", "Number", "Description", "Status", "Budget", "Title", "Deadline" FROM public."Phase" WHERE "JID" = '"""+JID+"""';"""
                phases = db.select(sql)
                for j in range(len(phases)):
                    currentPhase = phases[j]
                    if currentPhase[3] == "pending":
                        obj = {"JID": JID,"JobName": currentJob[1], "Number": currentPhase[1], "Budget": currentPhase[4], "Title": currentPhase[5], "Deadline": currentPhase[6]}
                        data.append(obj)
            return {"data": data, "code": 200}
