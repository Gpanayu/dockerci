from flask import session
from entity import User, Job
from database import db

class RequestControl:
    notLoginMsg = {"msg": "Please log in first", "code": 403}

    def __init__(self):
        # please delete this function
        print("created")


    def requestJob(self, req):
        if not 'UID' in session:
            return {"msg": "Please log in first", "code": 403}
        else:
            sql = """SELECT "JID", "Title" FROM public."Job" WHERE "JID" = '"""+req['JID']+"';"
            checkRes = db.select(sql)
            if len(checkRes) == 0:
                return {"msg": "No job in db", "code": 404}
            else:
                job = Job(req['JID'])
                job.request(session['UID'])
                del job
                return {"msg": "successful", "code": 201}


    def cancelRequestJob(self, req):
        if not 'UID' in session:
            return {"msg": "Please log in first", "code": 403}
        else:
            sql = """SELECT "JID", "Title" FROM public."Job" WHERE "JID" = '"""+req['JID']+"';"
            checkRes = db.select(sql)
            if len(checkRes) == 0:
                return {"msg": "No job in db", "code": 404}
            else:
                job = Job(req['JID'])
                if job.getStatus() == "Taken":
                    sql = """SELECT "UID" FROM public."TakeJob" WHERE "JID" = '"""+req['JID']+"';"
                    designer = db.select(sql)
                    del job
                    return {"msg": "already took the job", "code": 201}                            #when user already took the job
                else:
                    job.cancelRequestJob(session['UID'])
                del job
                return {"msg": "successful", "code": 201}


    def takeJob(self, req):
        if not 'UID' in session:
            return self.notLoginMsg
        else:
            UID = session['UID']
            JID = req['JID']
            try:
                #status "Available" indicates job owner haven't decided yet
                #status "Taken" indicates job already matched
                #status is a UID indicates job owner select designer with that UID
                job = Job(JID)
                status = job.getStatus()
                if status == "Available":
                    return {"msg": "Owner still not select", "code": 200}    #when owner didn't confirmTakeJob yet
                elif status == UID:
                    job.updateStatus("Taken")
                    sql = """INSERT INTO public."TakeJob"("UID", "JID") \
                             VALUES ('"""+str(UID)+"', '"+str(JID)+"');"
                    db.insert(sql)
                    return {"msg": "OK", "code": 200}
                elif status == "Taken": 
                    return {"msg": "job taken", "code":200}                 # when The job is already taken by other desinger
                return {"msg": "Owner did not select", "code":200}          # when Owner select someone else
            except :
                print("error")
                return {"msg": "error", "code":204}


    def confirmTakeJob(self, req):
        if not 'UID' in session:
            return self.notLoginMsg
        else:
            UID = session['UID']
            JID = req['JID']
            sql = """SELECT "UID" FROM "Request" WHERE "JID" = '"""+str(JID)+"';"
            query = db.select(sql)
            if len(query) == 0:
                return {"msg": "request empty", "code":200}                         #No one request for the job yet
            designerUID = req['UID']
            for jRequest in query:
                if designerUID in jRequest:
                    try:
                        job = Job(JID)
                        if job.getOwnerUID() != UID:
                            return {"msg": "no permission", "code":403}             #Current user not the job owner
                        status = job.getStatus()
                        if status == "Available":
                            job.updateStatus(designerUID)
                            return {"msg": "OK", "code": 200}
                        elif status == "Taken":
                            return {"msg": "job taken", "code":200}                 #When the job is already taken
                        return {"msg": "job status :"+status, "code":200}           #When already select a designer
                    except:
                        print("error")
                        return {"msg": "error", "code":204}
            return {"msg": "User did not request", "code":200}                      #when selected user did not request for the job

