from flask import session
from entity import ReportBank



class EvaluationControl:
    def __init__(self):
        # please delete this function
        print("created")


    def getReports(self):
        reportBank = ReportBank()
        try:
            reports = reportBank.getReports()
            res = []
            for report in reports:
                rep = {}
                rep['UID'] = report[0]
                rep['JID'] = report[1]
                rep['Date'] = report[2]
                rep['ComplainMsg'] = report[3]
                rep['Evidence'] = report[4]
                res.append(rep)
            
            return {"msg": res, "code": "200"}
        except:
            return {"msg": "error get report","code": "201"}
