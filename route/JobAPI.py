from flask import Blueprint, request, jsonify
from flask_json import json_response
from database import db
from controller import JobControl, RequestControl

job_api = Blueprint('job_api', __name__)

@job_api.route('/API/job/createJob', methods=['POST'])
def createJob():
    req = request.get_json()
    jobCT = JobControl()
    res = jobCT.createJob(req)
    code = res["code"]
    res = jsonify(res)
    del jobCT
    return res, code

@job_api.route('/API/job/editJob', methods=['POST'])
def editJob():
    req = request.get_json()
    jobCT = JobControl()
    res = jobCT.editJobDetail(req)
    code = res["code"]
    res = jsonify(res)
    del jobCT
    return res, code

@job_api.route('/API/job/cancelJob', methods=['POST'])
def cancelJob():
    req = request.get_json()
    jobCT = JobControl()
    res = jobCT.cancelJob(req)
    code = res["code"]
    res = jsonify(res)
    del jobCT
    return res, code

@job_api.route('/API/job/addJobPhase', methods=['POST'])
def addJobPhase():
    req = request.get_json()
    jobCT = JobControl()
    res = jobCT.addJobPhase(req)
    code = res["code"]
    res = jsonify(res)
    del jobCT
    return res, code

@job_api.route('/API/job/removeJobPhase', methods=['POST'])
def removeJobPhase():
    req = request.get_json()
    jobCT = JobControl()
    res = jobCT.removeJobPhase(req)
    code = res["code"]
    res = jsonify(res)
    del jobCT
    return res, code

@job_api.route('/API/job/clearAllJobPhase', methods=['POST'])
def clearAllJobPhase():
    req = request.get_json()
    jobCT = JobControl()
    res = jobCT.clearAllJobPhase(req)
    code = res["code"]
    res = jsonify(res)
    del jobCT
    return res, code

@job_api.route('/API/job/editPhaseJobDetail', methods=['POST'])
def editPhaseJobDetail():
    req = request.get_json()
    jobCT = JobControl()
    res = jobCT.editPhaseJobDetail(req)
    code = res["code"]
    res = jsonify(res)
    del jobCT
    return res, code

@job_api.route('/API/job/getRecentMyJob', methods=['POST'])
def getRecentMyJob():
    req = request.get_json()
    jobCT = JobControl()
    res = jobCT.getRecentMyJob(req)
    res = jsonify(res)
    del jobCT
    return res

@job_api.route('/API/job/getAllJob', methods=['POST'])
def getAllJob():
    req = request.get_json()
    jobCT = JobControl()
    res = jobCT.getAllJob(req)
    res = jsonify(res)
    del jobCT
    return res

@job_api.route('/API/job/getDesignerTakeNRequestJob', methods=['POST'])
def getDesignerTakeNRequestJob():
    req = request.get_json()
    jobCT = JobControl()
    res = jobCT.getDesignerTakeNRequestJob(req)
    res = jsonify(res)
    del jobCT
    return res

@job_api.route('/API/job/getJob', methods=['POST'])
def getJob():
    req = request.get_json()
    jobCT = JobControl()
    res = jobCT.getJob(req)
    res = jsonify(res)
    del jobCT
    return res

@job_api.route('/API/job/searchJob', methods=['POST'])
def searchJob():
    req = request.get_json()
    jobCT = JobControl()
    res = jobCT.searchJob(req)
    res = jsonify(res)
    del jobCT
    return res

@job_api.route('/API/job/getJobRT', methods=['POST'])
def getJobRT():
    req = request.get_json()
    jobCT = JobControl()
    res = jobCT.getJobRT(req)
    res = jsonify(res)
    del jobCT
    return res

@job_api.route('/API/job/requestJob', methods=['POST'])
def requestJob():
    req = request.get_json()
    reqCT = RequestControl()
    res = reqCT.requestJob(req)
    code = res["code"]
    res = jsonify(res)
    del reqCT
    return res, code

@job_api.route('/API/job/cancelRequestJob', methods=['DELETE'])
def cancelRequestJob():
    req = request.get_json()
    reqCT = RequestControl()
    res = reqCT.cancelRequestJob(req)
    code = res["code"]
    res = jsonify(res)
    del reqCT
    return res, code

@job_api.route('/API/job/takeJob', methods=['PATCH'])
def takeJob():
    req = request.get_json()
    reqCT = RequestControl()
    res = reqCT.takeJob(req)
    print(res)
    code = res["code"]
    res = jsonify(res)
    del reqCT
    return res, code

@job_api.route('/API/job/confirmTakeJob', methods=['PATCH'])
def confirmTakeJob():
    req = request.get_json()
    reqCT = RequestControl()
    res = reqCT.confirmTakeJob(req)
    code = res["code"]
    res = jsonify(res)
    del reqCT
    return res, code

@job_api.route('/API/job/getPendingPhase', methods=['GET'])
def getPendingPhase():
    jobCT = JobControl()
    res = jobCT.getPendingPhase()
    code = res["code"]
    res = jsonify(res)
    del jobCT
    return res, code
