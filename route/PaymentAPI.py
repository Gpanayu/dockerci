from controller import PaymentControl
from flask import Blueprint, request, jsonify
import stripe
from database import db

payment_api = Blueprint('payment_api', __name__)

@payment_api.route('/pay', methods=['POST'])
def pay():
    req = request.get_json()
    stripe.api_key = 'sk_test_kwd6ywczqCu8UlN8xfjeCjlX'
    pc = PaymentControl()
    customer = stripe.Customer.create(email='netluckae@hotmail.com')
    receipt, status = pc.payByCard(req, customer)
    del pc
    
    sql_count = '''SELECT count("ReceiptID") FROM public."CardReceipt";'''
    
    cur_id = db.select(sql)[0][0] + 1
    d = receipt.date
    amount = receipt.amount
    holder = receipt.holderName
    cc_num = receipt.cardNumber
    bank = receipt.bank
    
    sql_insert = '''INSERT INTO public."CardReceipt" ("ReceiptID", "Date", "Amount", "CardHolder", "CardNumber", "Bank") VALUES (''' + "'" + str(cur_id) + "','" + str(d) + "'," + str(amount) + ",'" + str(holder) + "','" + str(cc_num) + "','" + str(bank) + "');"
    
    db.insert(sql_insert)
    return receipt, status

@payment_api.route('/getUnverifiedPayment', methods=['GET'])
def getUnverifiedPayment():
    pc = PaymentControl()
    res = pc.getUnverifiedPayment()
    code = res["code"]
    del pc
    res = jsonify(res)
    return res, code

@payment_api.route('/getReceipts', methods=['GET'])
def getReceipts():
    pc = PaymentControl()
    res = pc.getReceipts()
    code = res["code"]
    del pc
    res = jsonify(res)
    return res, code
