from controller import TransactionControl
from flask import Blueprint, request, jsonify

transaction_api = Blueprint('transaction_api', __name__,url_prefix='/API/transaction')

@transaction_api.route('/showTransactions', methods=['GET'])
def showTransactions():
    transactionCT = TransactionControl()
    res = transactionCT.showTransactions()
    code = res["code"]
    res = jsonify(res)
    del transactionCT
    return res, code

@transaction_api.route('/removeTransaction', methods=['DELETE'])
def removeTransaction():
    req = request.get_json()
    transactionCT = TransactionControl()
    res = transactionCT.removeTransaction(req)
    code = res["code"]
    res = jsonify(res)
    del transactionCT
    return res, code

@transaction_api.route('/verifyReceipt', methods=['POST'])
def verifyReceipt():
    req = request.get_json()
    transactionCT = TransactionControl()
    res = transactionCT.verifyReceipt(req)
    code = res["code"]
    res = jsonify(res)
    del transactionCT
    return res, code

@transaction_api.route('/getReceipts', methods=['GET'])
def getReceipts():
    transactionCT = TransactionControl()
    res = transactionCT.getReceipts()
    code = res["code"]
    res = jsonify(res)
    del transactionCT
    return res, code

@transaction_api.route('/getTransactionDetail', methods=['GET'])
def getTransactionDetail():
    TID = request.args.get('TID')
    transactionCT = TransactionControl()
    res = transactionCT.getTransactionDetail(TID)
    code = res["code"]
    res = jsonify(res)
    del transactionCT
    return res, code
