from flask import Blueprint, request, jsonify
from flask_json import json_response
from database import db
from controller import AuthenticationControl
from route.mail import MailSender

authentication_api = Blueprint('authentication_api', __name__)
# ======= This section is for testing purpose only =======
@authentication_api.route('/test')
def index():
    # test = db.testConnection()
    test = db.select("""SELECT * FROM "Admin";""")
    return str(test)
@authentication_api.route('/testinsert')
def testinsert():
    sql = """INSERT INTO public."Admin"("AID", username, password) VALUES ('32323232323232322', 'test2', 'password');"""
    ret = db.insert(sql)
    return ret
@authentication_api.route('/testdelete')
def testdelete():
    sql = """DELETE FROM public."Admin" WHERE username='test2';"""
    ret = db.delete(sql)
    return ret
@authentication_api.route('/testupdate')
def testupdate():
    sql = """UPDATE public."Admin" SET username='mynameisoat2' WHERE "AID"='32323232323232322';"""
    ret = db.update(sql)
    return ret

# ====== This section is for the real app ======
@authentication_api.route('/register', methods=['POST'])
def register():
    req = request.get_json()
    authControl = AuthenticationControl()
    res = authControl.signup(req)
    code = res["code"]
    res = jsonify(res)
    del authControl
    return res, code

@authentication_api.route('/registerWithVer', methods=['POST'])
def registerWithVer():
    req = request.get_json()
    authControl = AuthenticationControl()
    res = authControl.signupWithVer(req)
    if res:
        code = 200
        email = res["Email"]
        res = {"msg": "Please check your email"}
        mailSender = MailSender()
        mailSender.send(email, 'Login In Verification email from Graphic designer platfrom', 'This email is sended to confirm your registration process.')
    else:
        code = 409
        res = {"msg": "Duplicate email or username"}
    res = jsonify(res)
    del authControl
    return res, code

@authentication_api.route('/login', methods=['POST'])
def login():
    req = request.get_json()
    authControl = AuthenticationControl()
    res = authControl.login(req)
    if not res:
        code = 401
        res = {"msg": "Wrong identification"}
    else:
        code = 200
    res = jsonify(res)
    del authControl
    return res, code

@authentication_api.route('/logout', methods=['POST'])
def logout():
    authControl = AuthenticationControl()
    res = authControl.logout()
    code = res["code"]
    res = jsonify(res)
    del authControl
    return res, code
