import smtplib
from getpass import getpass
from email.mime.text import MIMEText

class MailSender():
    def __init__(self):
        self.sender = 'se20182018graphic@gmail.com'
        self.smtp_server_name = 'smtp.gmail.com'
        self.port = '587' # for normal messages
        self.server = smtplib.SMTP('{}:{}'.format(self.smtp_server_name, self.port))
        self.server.ehlo()
        self.server.starttls()
        self.server.login(self.sender, 'Se20182018!')
        print('This MailSender is using SMTP protocol on port ' + self.port)

    def sendMail(self, receiver, subject, content):
        msg = MIMEText(content)
        msg['From'] = self.sender
        msg['To'] = receiver
        msg['Subject'] = subject
        self.server.send_message(msg)

    def closeServer(self):
        self.server.quit()