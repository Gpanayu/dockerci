from flask import Blueprint, request, jsonify
from controller import UserControl

user_api = Blueprint('user_api', __name__, url_prefix='/API/user')


@user_api.route('/getOwnProfile', methods=['GET'])
def getOwnProfile():
    userCT = UserControl()
    res = userCT.getProfile()
    if not res:
        code = 404
        res = {"msg": "User not fond"}
        res = jsonify(res)
    else :
        code = 200
        res = jsonify(res)
    del userCT
    return res, code

@user_api.route('/getProfile', methods=['POST'])
def getProfile():
    req = request.get_json()
    userCT = UserControl()
    res = userCT.getProfile(req["UID"])
    if not res:
        code = 404
        res = {"msg": "User not fond"}
        res = jsonify(res)
    else :
        code = 200
        res = jsonify(res)
    del userCT
    return res, code

@user_api.route('/updateProfile', methods=['POST'])
def updateProfile():
    req = request.get_json()
    userCT = UserControl()
    res = userCT.updateProfile(req)
    if not res:
        code = 401
        res = {"msg": "Wrong profile"}
        res = jsonify(res)
    else :
        code = 200
        res = {"msg": "Success"}
        res = jsonify(res)
    del userCT
    return res, code
