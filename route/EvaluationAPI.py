from flask import Blueprint, request, jsonify
from flask_json import json_response
from database import db
from controller import EvaluationControl

evaluation_api = Blueprint('evaluation_api', __name__, url_prefix='/API/evaluation')

@evaluation_api.route('getReports', methods=['GET'])
def getReports():
    evaluationCT = EvaluationControl()
    res = evaluationCT.getReports()
    code = res["code"]
    res = jsonify(res["msg"])
    del evaluationCT
    return res, code
