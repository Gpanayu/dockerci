from flask import Flask, render_template, request, session
from flask_cors import CORS, cross_origin
from secret import Email, Password
from flask_mail import Mail, Message

# ENCODING
import sys
import codecs
import os

from controller import AuthenticationControl, UserControl, RequestControl, JobControl, EvaluationControl, PaymentControl, TransactionControl

# sys.stdout = codecs.getwriter('utf8')(sys.stdout)
# sys.stderr = codecs.getreader('utf8')(sys.stderr)

app = Flask(__name__)
cors = CORS(app)
app.secret_key = os.urandom(24)
app.config.from_object('config')

mail_settings = {
    "MAIL_SERVER": 'smtp.gmail.com',
    "MAIL_PORT": 465,
    "MAIL_USE_TLS": False,
    "MAIL_USE_SSL": True,
    "MAIL_USERNAME": Email,
    "MAIL_PASSWORD": Password
}

app.config.update(mail_settings)
mail = Mail(app)

# interfaces  / APIs
from route.AuthenticationAPI import authentication_api
from route.UserAPI import user_api
from route.EvaluationAPI import evaluation_api
from route.JobAPI import job_api
from route.PaymentAPI import payment_api
from route.TransactionAPI import transaction_api
app.register_blueprint(authentication_api)
app.register_blueprint(user_api)
app.register_blueprint(evaluation_api)
app.register_blueprint(job_api)
app.register_blueprint(payment_api)
app.register_blueprint(transaction_api)

if __name__ == '__main__':
    app.run(debug=True)
