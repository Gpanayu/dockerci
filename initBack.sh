pip install -r /app/requirements.txt;
cp /app/secret.py /;
export FLASK_APP=/app/app;
export FLASK_ENV=development;
flask run --host='0.0.0.0';