-- please run these sql in pgAdmin 
-- user table
INSERT INTO public."User"("UID", "isVerified", "LastName", "Picture", "BankAccountNumber", "FirstName", "Age", "CitizenID", "Email", "Password", "Username", "PhoneNumber", "LineID", "FacebookLink", "FacebookID", "FacebookToken")
VALUES  ('00000', 'True', 'lastname', 'Picture', 'BankAccountNumber', 'FirstName', '22', 'CitizenID', 'Email', 'Password', 'Username', 'PhoneNumber', 'LineID', 'FacebookLink', 'FacebookID', 'FacebookToken'),
	    ('00001', 'True', 'greentea', 'Picture', 'BankAccountNumber', 'mango', '22', '2565318981', 'thisisthailand@Email.com', 'Password', 'ornbnk48', '0812355489', 'LineID', 'FacebookLink', 'FacebookID', 'FacebookToken'),
	    ('00002', 'False', 'coffee', 'Picture', 'BankAccountNumber', 'orange', '22', '1583245987', 'wtf@Email.com', 'Password', 'jinny', '0942163269', 'LineID', 'FacebookLink', 'FacebookID', 'FacebookToken'),
	    ('00003', 'True', 'โอเลี้ยง', 'Picture', 'BankAccountNumber', 'ประยุทธ', '22', '12356985643', 'dictator@Email.com', 'Password', 'mr.dictator', '0891253698', 'LineID', 'FacebookLink', 'FacebookID', 'FacebookToken'),
	    ('00004', 'False', 'นมเย็น', 'Picture', 'BankAccountNumber', 'ขนุน', '22', '1546815387', 'sleeping@Email.com', 'Password', 'ทุเรียน', '0612593647', 'LineID', 'FacebookLink', 'FacebookID', 'FacebookToken');

--job table
INSERT INTO public."Job"("JID", "Title", "Deadline", "CreatedDate", "Description", "Owner", "Status", "Budget")
VALUES  ('20180000', 'notitle', '2018-12-01', '2018-10-01', 'ง่วงนอนก็ไปนอน ออกแบบlogo', '00003', '11', '200'),
	    ('20180001', 'ออกแบบ', '2018-12-01', '2018-10-01', 'ออกแบบตราพรรค', '00001', '11', '256'),
	    ('20180002', 'วาดรูป', '2018-11-01', '2018-10-01', 'วาดรูปเสือดำ', '00003', '01', '10.25'),
	    ('20180003', 'ทำงานให้', '2018-12-23', '2018-10-01', 'โปรเจดเดือดมาก อยากได้คนมาทำให้', '00000', '10', '199'),
	    ('20180004', 'ทำโมเดล', '2018-12-01', '2018-10-01', 'ขอโมเดลอรอุ๋ง', '00002', '00', '100000');
	
--Phase
INSERT INTO public."Phase"(
	"JID", "Number", "Description", "Status", "Budget", "Title", "Deadline")
	VALUES ('20180000', '1', 'first part', 'avaliable', '1500', 'first draft', '2018-11-01'),
			('20180000', '2', 'final part', 'avaliable', '1500', 'fianl draft', '2018-12-01');

--Request
INSERT INTO public."Request"(
	"UID", "JID")
	VALUES ('00004', '20180000');