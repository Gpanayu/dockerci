--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5
-- Dumped by pg_dump version 10.5

-- Started on 2018-11-06 02:16:51

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12924)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2924 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 212 (class 1259 OID 24733)
-- Name: Address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Address" (
    "UID" character varying(256) NOT NULL,
    "Road" character varying(256) NOT NULL,
    "HouseNO" character varying(256) NOT NULL,
    "Soi" character varying(256) NOT NULL,
    "District" character varying(256) NOT NULL,
    "SubDistrict" character varying(256) NOT NULL,
    "Country" character varying(256) NOT NULL,
    "Province" character varying(256) NOT NULL,
    "ZipCode" character varying(256) NOT NULL
);


ALTER TABLE public."Address" OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 24577)
-- Name: Admin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Admin" (
    "AID" character varying(256) NOT NULL,
    username character varying(256) NOT NULL,
    password character varying(256) NOT NULL
);


ALTER TABLE public."Admin" OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 24624)
-- Name: Receipt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Receipt" (
    "ReceiptID" character varying(256) NOT NULL,
    "Date" date,
    "Amount" integer
);


ALTER TABLE public."Receipt" OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 24635)
-- Name: CardReceipt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."CardReceipt" (
    "CardHolder" character varying(256),
    "CardNumber" character varying(256),
    "Bank" character varying(256)
)
INHERITS (public."Receipt");


ALTER TABLE public."CardReceipt" OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 24649)
-- Name: Job; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Job" (
    "JID" character varying(256) NOT NULL,
    "Title" character varying(256),
    "Deadline" date,
    "CreatedDate" date,
    "Description" character varying(2048),
    "Owner" character varying(256),
    "Status" character varying(256),
    "Budget" double precision
);


ALTER TABLE public."Job" OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 24678)
-- Name: JobTypeEntity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."JobTypeEntity" (
    "JID" character varying(256) NOT NULL,
    "JobType" character varying(256) NOT NULL
);


ALTER TABLE public."JobTypeEntity" OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 24741)
-- Name: Phase; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Phase" (
    "JID" character varying(256) NOT NULL,
    "Number" character varying(256) NOT NULL,
    "Description" text,
    "Status" character varying(256),
    "Budget" double precision,
    "Title" character varying(256)
);


ALTER TABLE public."Phase" OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 24703)
-- Name: PostJob; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."PostJob" (
    "UID" character varying(256) NOT NULL,
    "Job" character varying(256) NOT NULL,
    "RatingForJob" character varying(256),
    "CommentForJob" text
);


ALTER TABLE public."PostJob" OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 24749)
-- Name: PreferredTypeEntity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."PreferredTypeEntity" (
    "UID" character varying(256) NOT NULL,
    "PreferredType" character varying(256) NOT NULL
);


ALTER TABLE public."PreferredTypeEntity" OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 24670)
-- Name: Receive; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Receive" (
    "UID" character varying(256) NOT NULL,
    "JID" character varying(256) NOT NULL
);


ALTER TABLE public."Receive" OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 24686)
-- Name: Report; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Report" (
    "UID" character varying(256) NOT NULL,
    "JID" character varying(256) NOT NULL,
    "Date" date,
    "ComplaintMessage" text,
    "Evidence" character varying(2048)
);


ALTER TABLE public."Report" OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 24695)
-- Name: Request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Request" (
    "UID" character varying(256) NOT NULL,
    "JID" character varying(256) NOT NULL
);


ALTER TABLE public."Request" OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 24629)
-- Name: SlipReceipt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SlipReceipt" (
    "isVerify" boolean,
    "toBankAccount" character varying(256),
    "fromBankAccount" character varying(256),
    "Slip" character varying(1024)
)
INHERITS (public."Receipt");


ALTER TABLE public."SlipReceipt" OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 24711)
-- Name: TakeJob; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."TakeJob" (
    "UID" character varying(256) NOT NULL,
    "JID" character varying(256) NOT NULL,
    "Deposit" double precision,
    "RatingForDesigner" character varying(256),
    "CommentForDesigner" text
);


ALTER TABLE public."TakeJob" OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 24616)
-- Name: Transaction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Transaction" (
    "TID" character varying(256) NOT NULL,
    "ReceiveID" character varying(256),
    "Type" character varying(256),
    "Amount" integer,
    "PayID" character varying(256),
    "DateTime" date
);


ALTER TABLE public."Transaction" OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 24717)
-- Name: TransactionPhase; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."TransactionPhase" (
    "TID" character varying(256) NOT NULL,
    "Number" character varying(256) NOT NULL,
    "JID" character varying(256) NOT NULL
);


ALTER TABLE public."TransactionPhase" OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 24725)
-- Name: TransactionReceipt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."TransactionReceipt" (
    "TID" character varying(256) NOT NULL,
    "ReceiptID" character varying(256) NOT NULL
);


ALTER TABLE public."TransactionReceipt" OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 24662)
-- Name: Transfer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Transfer" (
    "UID" character varying(256) NOT NULL,
    "TID" character varying(256) NOT NULL
);


ALTER TABLE public."Transfer" OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 24585)
-- Name: User; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."User" (
    "UID" character varying(256) NOT NULL,
    "isVerified" boolean,
    "LastName" character varying(256),
    "Picture" character varying(512),
    "BankAccountNumber" character varying(256),
    "FirstName" character varying(256),
    "Age" integer,
    "CitizenID" character varying(256),
    "Email" character varying(256),
    "Password" character varying(256),
    "Username" character varying(256),
    "PhoneNumber" character varying(256),
    "LineID" character varying(256),
    "FacebookLink" character varying(256),
    "FacebookID" character varying(512),
    "FacebookToken" character varying(2048)
);


ALTER TABLE public."User" OWNER TO postgres;

--
-- TOC entry 2791 (class 2606 OID 24740)
-- Name: Address Address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Address"
    ADD CONSTRAINT "Address_pkey" PRIMARY KEY ("UID", "Road", "HouseNO", "Soi", "District", "SubDistrict", "Country", "Province", "ZipCode");


--
-- TOC entry 2759 (class 2606 OID 24584)
-- Name: Admin Admin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Admin"
    ADD CONSTRAINT "Admin_pkey" PRIMARY KEY ("AID");


--
-- TOC entry 2769 (class 2606 OID 24762)
-- Name: CardReceipt CardReceipt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."CardReceipt"
    ADD CONSTRAINT "CardReceipt_pkey" PRIMARY KEY ("ReceiptID");


--
-- TOC entry 2777 (class 2606 OID 24685)
-- Name: JobTypeEntity JobTypeEntity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."JobTypeEntity"
    ADD CONSTRAINT "JobTypeEntity_pkey" PRIMARY KEY ("JID", "JobType");


--
-- TOC entry 2771 (class 2606 OID 24656)
-- Name: Job Job_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Job"
    ADD CONSTRAINT "Job_pkey" PRIMARY KEY ("JID");


--
-- TOC entry 2793 (class 2606 OID 24748)
-- Name: Phase Phase_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Phase"
    ADD CONSTRAINT "Phase_pkey" PRIMARY KEY ("JID", "Number");


--
-- TOC entry 2783 (class 2606 OID 24710)
-- Name: PostJob PostJob_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PostJob"
    ADD CONSTRAINT "PostJob_pkey" PRIMARY KEY ("UID", "Job");


--
-- TOC entry 2795 (class 2606 OID 24756)
-- Name: PreferredTypeEntity PreferredTypeEntity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PreferredTypeEntity"
    ADD CONSTRAINT "PreferredTypeEntity_pkey" PRIMARY KEY ("UID", "PreferredType");


--
-- TOC entry 2765 (class 2606 OID 24628)
-- Name: Receipt Receipt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Receipt"
    ADD CONSTRAINT "Receipt_pkey" PRIMARY KEY ("ReceiptID");


--
-- TOC entry 2775 (class 2606 OID 24677)
-- Name: Receive Receive_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Receive"
    ADD CONSTRAINT "Receive_pkey" PRIMARY KEY ("UID", "JID");


--
-- TOC entry 2779 (class 2606 OID 24693)
-- Name: Report Report_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Report"
    ADD CONSTRAINT "Report_pkey" PRIMARY KEY ("UID", "JID");


--
-- TOC entry 2781 (class 2606 OID 24702)
-- Name: Request Request_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Request"
    ADD CONSTRAINT "Request_pkey" PRIMARY KEY ("UID", "JID");


--
-- TOC entry 2767 (class 2606 OID 24760)
-- Name: SlipReceipt SlipReceipt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SlipReceipt"
    ADD CONSTRAINT "SlipReceipt_pkey" PRIMARY KEY ("ReceiptID");


--
-- TOC entry 2785 (class 2606 OID 24758)
-- Name: TakeJob TakeJob_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TakeJob"
    ADD CONSTRAINT "TakeJob_pkey" PRIMARY KEY ("UID", "JID");


--
-- TOC entry 2787 (class 2606 OID 24724)
-- Name: TransactionPhase TransactionPhase_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TransactionPhase"
    ADD CONSTRAINT "TransactionPhase_pkey" PRIMARY KEY ("TID", "Number", "JID");


--
-- TOC entry 2789 (class 2606 OID 24732)
-- Name: TransactionReceipt TransactionReceipt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TransactionReceipt"
    ADD CONSTRAINT "TransactionReceipt_pkey" PRIMARY KEY ("TID", "ReceiptID");


--
-- TOC entry 2763 (class 2606 OID 24623)
-- Name: Transaction Transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Transaction"
    ADD CONSTRAINT "Transaction_pkey" PRIMARY KEY ("TID");


--
-- TOC entry 2773 (class 2606 OID 24669)
-- Name: Transfer Transfer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Transfer"
    ADD CONSTRAINT "Transfer_pkey" PRIMARY KEY ("UID", "TID");


--
-- TOC entry 2761 (class 2606 OID 24592)
-- Name: User User_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."User"
    ADD CONSTRAINT "User_pkey" PRIMARY KEY ("UID");

-- edit table Phase by add Deadline Column

ALTER TABLE public."Phase"
    ADD COLUMN "Deadline" date;

-- Completed on 2018-11-06 02:16:53

--
-- PostgreSQL database dump complete
--

